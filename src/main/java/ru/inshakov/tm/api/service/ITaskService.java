package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

}
