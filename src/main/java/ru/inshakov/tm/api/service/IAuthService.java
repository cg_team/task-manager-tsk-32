package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.model.User;

public interface IAuthService {

    void checkRoles(@NotNull final Role... roles);

    @Nullable
    User getUser();

    @Nullable
    String getUserId();

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}

