package ru.inshakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IService<E> {

    @NotNull
    E changeStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    E changeStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    E changeStatusByName(@NotNull String userId, @Nullable String name, @Nullable Status status);

    void clear(@NotNull String userId);

    @NotNull
    E finishById(@NotNull String userId, @Nullable String id);

    @NotNull
    E finishByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    E finishByName(@NotNull String userId, @Nullable String name);

    @Nullable
    List<E> findAll(@NotNull String userId, @Nullable Comparator<E> comparator);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    E findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @Nullable String name);

    @Nullable
    E removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    E removeOneByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E startById(@NotNull String userId, @Nullable String id);

    @NotNull
    E startByIndex(@NotNull String userId, @Nullable Integer index);

    @NotNull
    E startByName(@NotNull String userId, @Nullable String name);

    @NotNull
    E updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    E updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);


}
