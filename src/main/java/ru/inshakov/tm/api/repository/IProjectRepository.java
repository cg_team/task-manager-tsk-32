package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
