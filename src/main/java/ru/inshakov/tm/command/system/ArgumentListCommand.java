package ru.inshakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public final class ArgumentListCommand extends AbstractCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "arg-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getArguments();
        commands.stream().filter(command -> Optional.ofNullable(command.arg()).isPresent()).forEach((command -> System.out.println(command.arg())));
    }
}
