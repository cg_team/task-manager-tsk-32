package ru.inshakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "App version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));
    }

}
