package ru.inshakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by id";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusId);
        @Nullable final Task task = serviceLocator.getTaskService().changeStatusById(userId, id, status);
    }

}
