package ru.inshakov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.EmptyFilePathException;

import java.io.FileOutputStream;
import java.util.Optional;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-json";
    }

    @NotNull
    @Override
    public String description() {
        return "save data to json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath = serviceLocator.getPropertyService().getFileJsonPath("fasterXml");
        Optional.ofNullable(filePath).orElseThrow(EmptyFilePathException::new);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
