package ru.inshakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "change password of user profile";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}

