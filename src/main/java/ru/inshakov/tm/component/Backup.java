package ru.inshakov.tm.component;

import lombok.SneakyThrows;
import ru.inshakov.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private static final int INTERVAL = 30;

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand("backup-load");
    }

    @Override
    public void run() {
        bootstrap.parseCommand("backup-save");
    }
}
