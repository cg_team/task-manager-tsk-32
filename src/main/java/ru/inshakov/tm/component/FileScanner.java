package ru.inshakov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.bootstrap.Bootstrap;
import ru.inshakov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private static final int INTERVAL = 3;

    private final Bootstrap bootstrap;

    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (final AbstractCommand command: bootstrap.getCommandService().getArgsCommands()) {
            commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        final File file = new File("./");
        for (final File item: file.listFiles()) {
            if (!item.isFile()) continue;
            final String name = item.getName();
            final String fileName = name.replaceFirst("[.][^.]+$", "");
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

}
