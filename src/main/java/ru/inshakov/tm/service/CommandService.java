package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.repository.ICommandRepository;
import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.api.service.ICommandService;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commandRepository.getCommandByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return commandRepository.getCommandByArg(arg);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgsCommands() {
        return commandRepository.getArgsCommands();
    }

}

