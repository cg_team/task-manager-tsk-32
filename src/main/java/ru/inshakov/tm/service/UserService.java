package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.api.repository.IUserRepository;
import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.api.service.IUserService;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.exception.empty.*;
import ru.inshakov.tm.exception.entity.UserNotFoundException;
import ru.inshakov.tm.exception.user.AccessDeniedException;
import ru.inshakov.tm.exception.user.EmailTakenException;
import ru.inshakov.tm.exception.user.LoginTakenException;
import ru.inshakov.tm.model.User;
import ru.inshakov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (!Optional.ofNullable(login).isPresent()) return false;
        return findByLogin(login) != null;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (!Optional.ofNullable(email).isPresent()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userRepository.add(user);
        return user;
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
        if (isLoginExists(login)) throw new LoginTakenException(login);
        if (isEmailExists(email)) throw new EmailTakenException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(role).orElseThrow(EmptyRoleException::new);
        @NotNull final User user = create(login, password);
        user.setRole(role);
    }

    @Override
    public void setPassword (@Nullable final String userId, @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        @Nullable final User user = findById(userId);
        Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = userRepository.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return;
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        final User user = userRepository.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return;
        user.setLocked(false);
    }

}
