package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.user.AccessDeniedException;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity implements Serializable {

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @Nullable
    private String description = "";

    @Nullable
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String userId;

    public String getUserId() {
        if (userId == null || userId.isEmpty()) {
            throw new AccessDeniedException();
        }
        return userId;
    }

    public String toString() {
        return getId() + ": " + name;
    }

}
