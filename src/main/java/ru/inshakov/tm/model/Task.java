package ru.inshakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractBusinessEntity  implements IWBS {

    @Nullable
    private String projectId = null;

}