package ru.inshakov.tm.exception.user;

import org.jetbrains.annotations.Nullable;

public final class EmailTakenException extends RuntimeException {

    public EmailTakenException(@Nullable final String email) {
        super("Error! Email ``" + email + "`` is already taken...");
    }

}
