package ru.inshakov.tm.exception.system;

public final class UndefinedErrorException extends RuntimeException {

    public UndefinedErrorException() {
        super("Undefined Error. please contact your administrator");
    }

}
