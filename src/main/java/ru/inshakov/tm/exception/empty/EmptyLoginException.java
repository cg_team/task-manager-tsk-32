package ru.inshakov.tm.exception.empty;

public final class EmptyLoginException extends RuntimeException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
