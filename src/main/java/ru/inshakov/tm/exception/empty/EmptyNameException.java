package ru.inshakov.tm.exception.empty;

public final class EmptyNameException extends RuntimeException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
